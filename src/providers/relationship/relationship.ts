import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { SERVER } from '../../app/app.component';
import { SharedPage } from '../../shared/shared';

/*
  Generated class for the RelationshipProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RelationshipProvider {
  loggedUser: any;
  token: any;

  constructor(
    private http: HttpClient,
    private storage: Storage
  ) {
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
    shared.getToken().then((val) => {
      this.setToken(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  setToken(token) {
    this.token = token;
  }

  followUser(body) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    let params = new HttpParams();
    params = params.append('id_logged_user', this.loggedUser.id)
    let options = { headers: headers, params: params };
    body['follower'] = this.loggedUser.id;
    let response = this.http.post(SERVER + '/relationship/relationship-api/', body, options);
    return response;
  }

  unfollowUser(idRelationship) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    let options = { headers: headers };
    let response = this.http.delete(SERVER + '/relationship/relationship-api/' + idRelationship.toString(), options);
    return response;
  }

  getFollowers(url, isNewPage, idFollowed, toSearch = null) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + this.token.key);
    let params = new HttpParams();
    if (!isNewPage) {
      if (toSearch) params = params.append('to_search', toSearch);
      else {
        params = params.append('id_followed', idFollowed);
        params = params.append('id_logged_user', this.loggedUser.id);
      }
    }
    let options = { headers: headers, params: params };
    let response = this.http.get(url, options);
    return response;
  }

  getFolloweds(url, isNewPage, idFollower, toSearch = null) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + this.token.key);
    let params = new HttpParams();
    if (!isNewPage) {
      if (toSearch) params = params.append('to_search', toSearch);
      else {
        params = params.append('id_follower', idFollower);
        params = params.append('id_logged_user', this.loggedUser.id);
      }
    }
    let options = { headers: headers, params: params };
    let response = this.http.get(url, options);
    return response;
  }

}
