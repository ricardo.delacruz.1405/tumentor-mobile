import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { SERVER } from '../../app/app.component';
import { SharedPage } from '../../shared/shared';

/*
  Generated class for the CoreProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CoreProvider {
  loggedUser: any;
  token: any;

  constructor(
    private http: HttpClient,
    private storage: Storage
  ) {
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
    shared.getToken().then((val) => {
      this.setToken(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  setToken(token) {
    this.token = token;
  }

  loginUser(body) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    let options = { headers: headers };
    let response = this.http.post(SERVER + '/login-api/', body, options);
    return response;
  }

  createAccount(body) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    let options = { headers: headers };
    let response = this.http.post(SERVER + '/signup-api/', body, options);
    return response;
  }

  validateEmail(email) {
    let params = new HttpParams();
    params = params.append('email', email);
    let options = { params: params };
    let response = this.http.get(SERVER + '/email-api/', options);
    return response;
  }

  searchUsers(toSearch) {
    let params = new HttpParams();
    params = params.append('to_search', toSearch);
    let options = { params: params };
    let response = this.http.get(SERVER + '/users-api/', options);
    return response;
  }

  getUsers(url, isNewPage, toSearch = null) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    //headers = headers.append('Authorization', 'Bearer ' + this.decryptToken);
    let params = new HttpParams();
    if (!isNewPage) {
      if (toSearch) params = params.append('to_search', toSearch);
    }
    let options = { headers: headers, params: params };
    let response = this.http.get(url, options);
    return response;
  }

  getDataUser(idUser) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    let params = new HttpParams();
    params = params.append('id_logged_user', this.loggedUser.id);
    let options = { headers: headers, params: params };
    let response = this.http.get(SERVER + '/users-api/' + idUser.toString(), options);
    return response;
  }

}
