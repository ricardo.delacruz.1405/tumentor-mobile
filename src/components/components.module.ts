import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PreloadImageComponent } from './preload-image/preload-image';
@NgModule({
	declarations: [PreloadImageComponent],
	imports: [],
	exports: [PreloadImageComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
