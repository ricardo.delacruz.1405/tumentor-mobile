import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as CryptoJS from 'crypto-js';

/**
 * Generated class for the SharedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-shared',
  templateUrl: 'shared.html',
})
export class SharedPage {
  loggedUser: any;

  constructor(
    private storage: Storage
  ) {
  }

  encrypt(message, secretKey) {
    let encryptMessage = CryptoJS.AES.encrypt(message, secretKey);
    return encryptMessage;
  }

  decrypt(encryptMessage, secretKey) {
    let bytes = CryptoJS.AES.decrypt(encryptMessage, secretKey);
    let decryptMessage = bytes.toString(CryptoJS.enc.Utf8);
    return decryptMessage;
  }

  encryptObject(object, secretKey) {
    let encryptMessage = CryptoJS.AES.encrypt(JSON.stringify(object), secretKey);
    return encryptMessage;
  }

  decryptObject(encryptObject, secretKey) {
    let bytes = CryptoJS.AES.decrypt(encryptObject.toString(), secretKey);
    let decryptObject = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptObject;
  }

  getLoggedUser() {
    return this.storage.get('user').then((val) => {
      let loggedUser = null;
      if (val) {
        loggedUser = {
          id: this.decrypt(val.id, 'TuMentorApp'),
          first_name: val.first_name,
          last_name: val.last_name,
          image: val.image
        }
      }
      return loggedUser
    });
  }

  getToken() {
    return this.storage.get('token').then((val) => {
      let token = {
        key: this.decrypt(val.key, 'TuMentorApp')
      }
      return token
    });
  }

}
