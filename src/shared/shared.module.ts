import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedPage } from './shared';
import { BackgroundImage } from './components/background-image/background-image';
import { ColorRadio } from './components/color-radio/color-radio';
import { CounterInput } from './components/counter-input/counter-input';
import { GoogleMap } from './components/google-map/google-map';
import { PreloadImage } from './components/preload-image/preload-image';
import { Rating } from './components/rating/rating';
import { ShowHideInput } from './components/show-hide-password/show-hide-input';
import { ShowHideContainer } from './components/show-hide-password/show-hide-container';
import { ElasticTextAreaComponent } from './components/elastic-text-area/elastic-text-area';
import { ExpandableHeaderComponent } from './components/expandable-header/expandable-header';
import { ProgressBarComponent } from './components/progress-bar/progress-bar';

@NgModule({
  declarations: [
    SharedPage,
    BackgroundImage,
    ColorRadio,
    CounterInput,
    GoogleMap,
    PreloadImage,
    Rating,
    ShowHideInput,
    ShowHideContainer,
    ElasticTextAreaComponent,
    ExpandableHeaderComponent,
    ProgressBarComponent
  ],
  imports: [
    IonicPageModule.forChild(SharedPage),
  ],
  exports: [
    SharedPage,
    BackgroundImage,
    ColorRadio,
    CounterInput,
    GoogleMap,
    PreloadImage,
    Rating,
    ShowHideInput,
    ShowHideContainer,
    ElasticTextAreaComponent,
    ExpandableHeaderComponent,
    ProgressBarComponent
  ]
})
export class SharedModule { }