import { Component, Input, ViewChild } from '@angular/core';

/**
 * Generated class for the ElasticTextAreaComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'elastic-text-area',
  templateUrl: 'elastic-text-area.html'
})
export class ElasticTextAreaComponent {
  @ViewChild('ionTxtArea') ionTxtArea: any;
  @Input() placeholder: string;
  @Input() lineHeight: string;
  content: any;
  txtArea: any;

  constructor() {
    this.content = "";
    this.lineHeight = "22px";
  }

  ngAfterViewInit() {
    this.txtArea = this.ionTxtArea._elementRef.nativeElement.children[0];
    this.txtArea.style.height = this.lineHeight + "px";
  }

  onChange(newValue) {
    this.txtArea.style.height = this.lineHeight + "px";
    this.txtArea.style.height = this.txtArea.scrollHeight + "px";
  }

}
