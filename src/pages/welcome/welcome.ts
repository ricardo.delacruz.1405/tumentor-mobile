import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';


/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(
    private navCtrl: NavController,
    private splashScreen: SplashScreen,
    private androidFullScreen: AndroidFullScreen
  ) {
    
  }

  ionViewWillEnter() {
    this.androidFullScreen.isImmersiveModeSupported()
      .then(() => {
        this.androidFullScreen.showUnderStatusBar();
        this.androidFullScreen.showUnderSystemUI();
      })
      .catch(err => console.log(err));
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 600);
  }

  ionViewWillLeave() {
    this.androidFullScreen.showSystemUI();
  }

  goToSignup() {
    this.navCtrl.push('SignupPage');
  }

  goToLogin() {
    this.navCtrl.push('LoginPage');
  }

}
