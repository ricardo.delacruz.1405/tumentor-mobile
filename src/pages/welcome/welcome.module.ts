import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomePage } from './welcome';

import { ComponentsModule } from '../../components/components.module';

import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

@NgModule({
  declarations: [
    WelcomePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(WelcomePage),
  ],
  providers: [
    SplashScreen,
    AndroidFullScreen
  ]
})
export class WelcomePageModule { }
