import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabFollowedsPage } from './tab-followeds';

import { SharedModule } from '../../shared/shared.module';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    TabFollowedsPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(TabFollowedsPage),
  ],
  providers: [
    Toast,
    Network
  ]
})
export class TabFollowedsPageModule {}
