import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

import { CoreProvider } from '../../providers/core/core';

import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  keyboardIsOpen: boolean;
  signupForm: FormGroup;
  // valueProfile: string;
  gender: string;
  isLoading: boolean;

  constructor(
    private navCtrl: NavController,
    private platform: Platform,
    private formBuilder: FormBuilder,
    private storage: Storage,
    private toast: Toast,
    private network: Network,
    private coreProvider: CoreProvider
  ) {
    this.isLoading = false;
    this.keyboardIsOpen = false;
    // this.valueProfile = '1';
    this.gender = '1';
    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {

  }

  doSignup() {
    let isValid: boolean = true;

    if (!this.signupForm.controls.firstName.valid) {
      isValid = false;
      this.showToast('Ingresa tu nombre');
    } else if (!this.signupForm.controls.lastName.valid) {
      isValid = false;
      this.showToast('Ingresa tus apellidos');
    } else if (!this.signupForm.controls.email.valid) {
      isValid = false;
      this.showToast('Ingresa un correo electrónico válido');
    } else if (!this.signupForm.controls.password.valid) {
      isValid = false;
      this.showToast('Ingresa una contraseña');
    }

    if (isValid) {
      if (this.network.type == 'none') {
        this.showToast('Conéctate a Internet', 4500);
      } else {
        this.isLoading = true;
        // Validate email
        this.coreProvider.validateEmail(this.signupForm.controls.email.value).subscribe(
          data => {
            if (data) {
              if (data['exists']) {
                this.showToast('El email ya está registrado', 5500);
                this.isLoading = false;
              } else {
                // Create account
                let body = {
                  first_name: this.signupForm.controls.firstName.value,
                  last_name: this.signupForm.controls.lastName.value,
                  email: this.signupForm.controls.email.value,
                  password: this.signupForm.controls.password.value,
                  // value_profile: this.valueProfile
                  gender: this.gender
                }
                this.coreProvider.createAccount(body).subscribe(
                  data => {
                    if (data) {
                      let token = data['token'];
                      let user = data['user'];
                      let objShared = new SharedPage(this.storage);
                      token.key = objShared.encrypt(token.key, 'TuMentorApp').toString();
                      user.id = objShared.encrypt(user.id.toString(), 'TuMentorApp').toString();
                      this.storage.set('token', token);
                      this.storage.set('user', user);
                      this.navCtrl.setRoot('TabsHomePage');
                    }
                  },
                  err => {
                    this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
                    this.isLoading = false;
                  }
                )
              }
            } else {
              this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
              this.isLoading = false;
            }
          },
          err => {
            this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
            this.isLoading = false;
          }
        )
      }
    }
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

}
