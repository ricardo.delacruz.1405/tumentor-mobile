import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    SignupPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
  ],
  providers: [
    Toast,
    Network
  ]
})
export class SignupPageModule {}
