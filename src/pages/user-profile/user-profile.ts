import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';

import { CoreProvider } from '../../providers/core/core';
import { RelationshipProvider } from '../../providers/relationship/relationship';
import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  isLoading: boolean;
  updateRelationship: boolean;
  user: any;
  loggedUser: any;
  isMyProfile: boolean;

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private navParams: NavParams,
    private toast: Toast,
    private network: Network,
    private photoViewer: PhotoViewer,
    private coreProvider: CoreProvider,
    private relationshipProvider: RelationshipProvider
  ) {
    this.isLoading = true;
    this.updateRelationship = true;
    this.isMyProfile = false;
    this.user = {
      first_name: '',
      last_name: ''
    }
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });

  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
    this.getDataUser(this.navParams.get('idUser'));
  }

  getDataUser(idUser, refresher = null) {
    if (idUser) {
      if (this.network.type === 'none') {
        this.showToast('Conéctate a Internet', 4500);
      } else {
        this.coreProvider.getDataUser(idUser).subscribe(
          data => {
            this.user = data;
            if (this.user.id == this.loggedUser.id) this.isMyProfile = true;
            this.isLoading = false;
            if (refresher) refresher.complete();
          },
          err => {
            this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
            this.navCtrl.pop();
          }
        )
      }
    } else {
      this.showToast('No se encontró al usuario. Inténtelo más tarde');
      this.navCtrl.pop();
    }
  }

  followUser() {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      this.updateRelationship = false;
      this.user.num_followers += 1;
      this.user.relationship.is_following = !this.user.relationship.is_following;
      let body = {
        followed: this.user.id
      }
      this.relationshipProvider.followUser(body).subscribe(
        data => {
          data['updateRelationship'] = true;
          data['isVisible'] = true;
          this.user.relationship.id = data['id'];
          this.updateRelationship = true;
          let userParams = this.navParams.get('user');
          let textFolloweds = this.navParams.get('textFolloweds');
          let relationships = this.navParams.get('relationships');
          let noResults = this.navParams.get('noResults');
          let removeFromList = this.navParams.get('removeFromList');
          if (userParams && textFolloweds && relationships && noResults && removeFromList != null) {
            let foundItem;
            if (userParams.id == this.loggedUser.id) {
              foundItem = relationships.results.filter((item) => {
                if (item.follower == this.user.id) return true
              })
            } else {
              if (removeFromList == true) {
                foundItem = relationships.results.filter((item) => {
                  if (item.followed == this.user.id) return true
                })
              } else {
                foundItem = relationships.results.filter((item) => {
                  if (item.follower == this.user.id) return true
                })
              }

            }

            if (foundItem.length > 0) {
              foundItem = foundItem[0];

              if (this.loggedUser.id == foundItem.followed) {
                foundItem.obj_follower.relationship.id = data['id'];
                foundItem.obj_follower.relationship.is_following = !foundItem.obj_follower.relationship.is_following;

                userParams.num_followeds += 1;
                textFolloweds.content = userParams.num_followeds.toString();
                if (userParams.num_followeds == 1) textFolloweds.content += ' seguido';
                else textFolloweds.content += ' seguidos';

                this.storage.get('addedFolloweds').then((val) => {
                  if (val) {
                    let foundFollowed = val.filter((item) => {
                      if (item.follower == data['follower'] && item.followed == data['followed']) return true
                    })
                    if (foundFollowed.length == 0) {
                      val.push(data);
                      this.storage.set('addedFolloweds', val);
                    }

                  } else {
                    this.storage.set('addedFolloweds', [data]);
                  }
                });
                this.storage.remove('removedFolloweds');
              } else {
                if (removeFromList == true) {
                  foundItem.obj_followed.relationship.id = data['id'];
                  foundItem.obj_followed.relationship.is_following = !foundItem.obj_followed.relationship.is_following;
                } else {
                  foundItem.obj_follower.relationship.id = data['id'];
                  foundItem.obj_follower.relationship.is_following = !foundItem.obj_follower.relationship.is_following;
                }
                this.storage.get('updateStatus').then((val) => {
                  if (val) {
                    let foundRelationship = val.filter((item) => {
                      if (item.id == data['id']) return true
                    })
                    if (foundRelationship.length == 0) {
                      val.push(data);
                      this.storage.set('updateStatus', val);
                    }
                  } else {
                    this.storage.set('updateStatus', [data]);
                  }
                });
              }
            }


          }
        },
        err => {
          this.user.num_followers -= 1;
          this.user.relationship.is_following = !this.user.relationship.is_following;
          this.updateRelationship = true;
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
        }
      )
    }
  }

  unfollowUser() {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      this.updateRelationship = false;
      this.user.num_followers -= 1;
      this.user.relationship.is_following = !this.user.relationship.is_following;
      this.relationshipProvider.unfollowUser(this.user.relationship.id).subscribe(
        data => {
          this.updateRelationship = true;
          let userParams = this.navParams.get('user');
          let textFolloweds = this.navParams.get('textFolloweds');
          let relationships = this.navParams.get('relationships');
          let noResults = this.navParams.get('noResults');
          let removeFromList = this.navParams.get('removeFromList');

          if (userParams && textFolloweds && relationships && noResults && removeFromList != null) {
            let foundItem;
            if (removeFromList == true) {
              foundItem = relationships.results.filter((item) => {
                if (item.followed == this.user.id) return true
              })
            } else {
              foundItem = relationships.results.filter((item) => {
                if (item.follower == this.user.id) return true
              })
            }

            if (userParams.id == this.loggedUser.id) {
              userParams.num_followeds -= 1;

              if (foundItem.length > 0) {
                foundItem = foundItem[0];
                if (removeFromList == true) {
                  let index = relationships.results.indexOf(foundItem);
                  relationships.results.splice(index, 1);
                  if (relationships.results.length == 0) noResults.value = true;
                } else {
                  foundItem.obj_follower.relationship.is_following = !foundItem.obj_follower.relationship.is_following;
                }
              }
              
              textFolloweds.content = userParams.num_followeds.toString();
              if (userParams.num_followeds == 1) textFolloweds.content += ' seguido';
              else textFolloweds.content += ' seguidos';

              this.storage.get('removedFolloweds').then((val) => {
                if (val) {
                  val.push(this.user);
                  this.storage.set('removedFolloweds', val);
                } else {
                  this.storage.set('removedFolloweds', [this.user]);
                }
              });
              this.storage.remove('addedFolloweds');
            } else {
              if (foundItem.length > 0) {
                foundItem = foundItem[0];
                if (removeFromList == true) {
                  foundItem.obj_followed.relationship.id = null;
                  foundItem.obj_followed.relationship.is_following = !foundItem.obj_followed.relationship.is_following;
                } else {
                  foundItem.obj_follower.relationship.id = null;
                  foundItem.obj_follower.relationship.is_following = !foundItem.obj_follower.relationship.is_following;
                }

                this.storage.get('deleteRelationship').then((val) => {
                  if (val) {
                    let foundRelationship = val.filter((item) => {
                      if (item.id == foundItem.id) return true
                    })
                    if (foundRelationship.length == 0) {
                      val.push(foundItem);
                      this.storage.set('deleteRelationship', val);
                    }
                  } else {
                    this.storage.set('deleteRelationship', [foundItem]);
                  }
                });
              }
            }
          }
          this.user.relationship.id = null;
        },
        err => {
          this.user.num_followers += 1;
          this.user.relationship.is_following = !this.user.relationship.is_following;
          this.updateRelationship = true;
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
        }
      )
    }
  }

  goRelationships(indexTab) {
    let textFollowers = this.user.num_followers.toString();
    let textFolloweds = this.user.num_followeds.toString();
    if (this.user.num_followers == 1) textFollowers += ' seguidor';
    else textFollowers += ' seguidores';
    if (this.user.num_followeds == 1) textFolloweds += ' seguido';
    else textFolloweds += ' seguidos';

    this.navCtrl.push('TabsRelationshipPage', {
      rootUser: this.navParams.get('rootUser'),
      rootTextFolloweds: this.navParams.get('rootTextFolloweds'),
      user: this.user,
      textFollowers: { content: textFollowers },
      textFolloweds: { content: textFolloweds },
      indexTab: indexTab
    })
  }

  doRefresh(refresher) {
    this.getDataUser(this.user.id, refresher);
  }

  goEditProfile() {
    this.navCtrl.push('EditProfilePage', {
      user: this.user
    })
  }

  showImage(urlImage) {
    this.photoViewer.show(urlImage, this.user.first_name + ' ' + this.user.last_name);
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

}
