import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfilePage } from './user-profile';

import { SharedModule } from '../../shared/shared.module';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@NgModule({
  declarations: [
    UserProfilePage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(UserProfilePage),
  ],
  providers: [
    Toast,
    Network,
    PhotoViewer
  ]
})
export class UserProfilePageModule {}
