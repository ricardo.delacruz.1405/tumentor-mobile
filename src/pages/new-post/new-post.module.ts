import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewPostPage } from './new-post';

import { SharedModule } from '../../shared/shared.module';

import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    NewPostPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(NewPostPage),
  ],
  providers: [
    Camera
  ]
})
export class NewPostPageModule {}
