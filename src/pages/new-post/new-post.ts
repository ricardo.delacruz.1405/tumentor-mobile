import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the NewPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-post',
  templateUrl: 'new-post.html',
})
export class NewPostPage {
  loggedUser: any;
  image: any;
  canShare: boolean;
  postContent: string;
  placeholder: string;
  postWithImage: boolean;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private storage: Storage,
    private camera: Camera
  ) {
    this.canShare = false;
    this.postWithImage = false;
    this.postContent = '';
    this.placeholder = '¿Sobre qué quieres hablar?';
    this.loggedUser = {
      image: ''
    };
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  ionViewDidLoad() {

  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
      this.postWithImage = true;
      this.placeholder = 'Haz un comentario sobre la foto...';
      this.canShare = true
    }, (err) => {
      // Handle error
    });
  }

  chooseImage() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // this.image = imageData;
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
      this.postWithImage = true;
      this.placeholder = 'Haz un comentario sobre la foto...';
      this.canShare = true
    }, (err) => {
      // Handle error
    });
  }

  removeImage() {
    this.image = null;
    this.postWithImage = false;
    this.placeholder = '¿Sobre qué quieres hablar?';
    this.validatePost();
  }

  validatePost() {
    if (this.postContent.trim() == '') this.canShare = false;
    else this.canShare = true;
  }

  sharePost() {

  }

}
