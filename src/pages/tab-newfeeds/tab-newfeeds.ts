import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabNewfeedsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-newfeeds',
  templateUrl: 'tab-newfeeds.html',
})
export class TabNewfeedsPage {
  rootNavCtrl: NavController;
  loggedUser: any;

  constructor(
    private navParams: NavParams,
    private storage: Storage
  ) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
  }

  ionViewDidLoad() {
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  logout() {
    this.storage.remove('user');
    this.storage.remove('token');
    this.rootNavCtrl.setRoot('WelcomePage');
  }

  goNewPost() {
    this.rootNavCtrl.push('NewPostPage');
  }

}
