import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabNewfeedsPage } from './tab-newfeeds';

@NgModule({
  declarations: [
    TabNewfeedsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabNewfeedsPage),
  ],
})
export class TabNewfeedsPageModule {}
