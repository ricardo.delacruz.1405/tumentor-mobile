import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabAccountPage } from './tab-account';

import { SharedModule } from '../../shared/shared.module';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@NgModule({
  declarations: [
    TabAccountPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(TabAccountPage),
  ],
  providers: [
    Toast,
    Network,
    PhotoViewer
  ]
})
export class TabAccountPageModule {}
