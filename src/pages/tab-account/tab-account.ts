import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Storage } from '@ionic/storage';

import { CoreProvider } from '../../providers/core/core';
import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-account',
  templateUrl: 'tab-account.html',
})
export class TabAccountPage {
  rootNavCtrl: NavController;
  isLoading: boolean;
  user: any;
  userParams: any;
  loggedUser: any;

  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    private toast: Toast,
    private network: Network,
    private photoViewer: PhotoViewer,
    private coreProvider: CoreProvider,
    private storage: Storage
  ) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    if (this.rootNavCtrl == undefined) this.rootNavCtrl = this.navCtrl;
    this.userParams = this.navParams.get('user');
    this.user = {
      first_name: '',
      last_name: ''
    }
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
    this.getDataUser(this.loggedUser.id);
  }

  ionViewWillEnter() {
    this.isLoading = true;
    if (this.loggedUser) this.getDataUser(this.loggedUser.id);
  }

  getDataUser(idUser, refresher=null) {
    if (idUser) {
      if (this.network.type === 'none') {
        this.showToast('Conéctate a Internet', 4500);
      } else {
        this.coreProvider.getDataUser(idUser).subscribe(
          data => {
            this.user = data;
            this.isLoading = false;
            if (refresher) refresher.complete();
          },
          err => {
            this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
            this.rootNavCtrl.pop();
          }
        )
      }
    } else {
      this.showToast('No se encontró al usuario. Inténtelo más tarde');
      this.rootNavCtrl.pop();
    }
  }

  goEditProfile() {
    this.rootNavCtrl.push('EditProfilePage', {
      user: this.user
    })
  }

  goRelationships(indexTab) {
    let textFollowers = this.user.num_followers.toString();
    let textFolloweds = this.user.num_followeds.toString();
    if (this.user.num_followers == 1) textFollowers += ' seguidor';
    else textFollowers += ' seguidores';
    if (this.user.num_followeds == 1) textFolloweds += ' seguido';
    else textFolloweds += ' seguidos';

    let rootTextFolloweds = {value: textFolloweds}
    this.rootNavCtrl.push('TabsRelationshipPage', {
      rootUser: this.user,
      rootTextFolloweds: {content: rootTextFolloweds.value},
      user: this.user,
      loggedUser: this.user,
      textFollowers: {content: textFollowers},
      textFolloweds: {content: textFolloweds},
      indexTab: indexTab
    })
  }

  doRefresh(refresher) {
    this.getDataUser(this.loggedUser.id, refresher);
  }

  showImage(urlImage) {
    this.photoViewer.show(urlImage, this.user.first_name + ' ' + this.user.last_name);
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

}
