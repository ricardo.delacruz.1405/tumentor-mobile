import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsHomePage } from './tabs-home';

import { SharedModule } from '../../shared/shared.module';
import { SuperTabsModule } from 'ionic2-super-tabs';

import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    TabsHomePage,
  ],
  imports: [
    SharedModule,
    SuperTabsModule,
    IonicPageModule.forChild(TabsHomePage),
  ],
  providers: [
    SplashScreen
  ]
})
export class TabsHomePageModule {}
