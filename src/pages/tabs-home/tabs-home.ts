import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SplashScreen } from '@ionic-native/splash-screen';

import { SuperTabsController } from 'ionic2-super-tabs';
import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-home',
  templateUrl: 'tabs-home.html',
  providers: [SuperTabsController]
})
export class TabsHomePage {
  //ViewChild('searchBar') searchBar: any;
  loggedUser: any;
  tab1Root = 'TabNewfeedsPage';
  tab2Root = 'TabSearchPage';
  tab3Root = 'TabNewPostPage';
  tab4Root = 'TabNotificationsPage';
  tab5Root = 'TabAccountPage';
  params: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private storage: Storage,
    private splashScreen: SplashScreen
  ) {
    this.params = this.navParams.data;
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.splashScreen.hide();
      //this.searchBar._searchbarInput.nativeElement.readOnly = true;
    }, 600);
  }

  goSearch() {
    this.navCtrl.push('SearchUsersPage');
  }

  goProfile() {
    this.navCtrl.push('UserProfilePage', {
      idUser: this.loggedUser.id
    });
  }

  logout() {
    this.storage.remove('user');
    this.storage.remove('token');
    this.navCtrl.setRoot('WelcomePage');
  }

}
