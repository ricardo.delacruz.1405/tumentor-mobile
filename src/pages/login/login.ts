import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
// import { Keyboard } from '@ionic-native/keyboard';

import { CoreProvider } from '../../providers/core/core';

import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  keyboardIsOpen: boolean;
  loginForm: FormGroup;
  // valueProfile: string;
  isLoading: boolean;

  constructor(
    private navCtrl: NavController,
    private platform: Platform,
    private formBuilder: FormBuilder,
    private storage: Storage,
    private toast: Toast,
    private network: Network,
    // private keyboard: Keyboard,
    private coreProvider: CoreProvider
  ) {
    this.isLoading = false;
    this.keyboardIsOpen = false;
    // this.valueProfile = '1';
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    
  }

  doLogin() {
    let isValid: boolean = true;

    if (!this.loginForm.controls.email.valid) {
      isValid = false;
      this.showToast('Ingresa un correo electrónico válido');
    } else if (!this.loginForm.controls.password.valid) {
      isValid = false;
      this.showToast('Ingresa una contraseña');
    }

    if (isValid) {
      if (this.network.type == 'none') {
        this.showToast('Conéctate a Internet', 4500);
      } else {
        this.isLoading = true;
        let body = {
          email: this.loginForm.controls.email.value,
          password: this.loginForm.controls.password.value,
          // value_profile: this.valueProfile
        }
        this.coreProvider.loginUser(body).subscribe(
          data => {
            if (data) {
              let token = data['token'];
              let user = data['user'];
              let objShared = new SharedPage(this.storage);
              token.key = objShared.encrypt(token.key, 'TuMentorApp').toString();
              user.id = objShared.encrypt(user.id.toString(), 'TuMentorApp').toString();
              this.storage.set('token', token);
              this.storage.set('user', user);
              this.navCtrl.setRoot('TabsHomePage');
            } else {
              this.showToast('Email o contraseña incorrectos', 4500);
              this.isLoading = false;
            }
          },
          err => {
            this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
            this.isLoading = false;
          }
        )
      }
    }
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

  /*
  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.subscribeToKeyboardEvents();
    });
  }

  subscribeToKeyboardEvents(): void {
    this.keyboard.onKeyboardWillShow().subscribe(() => {
      this.keyboardIsOpen = true;
    });

    this.keyboard.onKeyboardWillHide().subscribe(() => {
      this.keyboardIsOpen = false;
    });
  }
  */

}
