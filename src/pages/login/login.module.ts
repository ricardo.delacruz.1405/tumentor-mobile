import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
// import { Keyboard } from '@ionic-native/keyboard';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
  providers: [
    Toast,
    Network
    // Keyboard
  ]
})
export class LoginPageModule {}
