import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

import { CoreProvider } from '../../providers/core/core';

import { SERVER } from '../../app/app.component';

/**
 * Generated class for the SearchUsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-users',
  templateUrl: 'search-users.html',
})
export class SearchUsersPage {
  @ViewChild('searchBar') searchBar: any;
  showInitialContent: boolean;
  isLoading: boolean;
  noResults: boolean;
  users: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private toast: Toast,
    private network: Network,
    private coreProvider: CoreProvider
  ) {
    this.showInitialContent = true;
    this.isLoading = true;
    this.users = {
      results: []
    };
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.searchBar._searchbarInput.nativeElement.focus();
    }, 800);
  }

  getItems(ev: any) {
    const val = ev.target.value;
    if (val) {
      if (val == '') this.showInitialContent = true;
      else {
        this.showInitialContent = false;
        let url = SERVER + '/users-api/';
        this.searchUsers(url, false, val.trim());
      }
    } else {
      this.showInitialContent = true;
    }
  }

  searchUsers(url, isNewPage, text) {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      this.isLoading = true;
      this.coreProvider.getUsers(url, isNewPage, text).subscribe(
        data => {
          if (this.users.next) {
            let newUsers = data['results'];
            this.users.next = data['next'];
            for (let user of newUsers) {
              this.users.results.push(user);
            }
          } else {
            this.users = data;
          }
          if (this.users.results.length == 0) this.noResults = true;
          else this.noResults = false;
          this.isLoading = false;
        },
        err => {
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
        }
      );
    }
  }

  goProfile(user) {
    this.navCtrl.push('UserProfilePage', {
      idUser: user.id,
      user: user
    });
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

}
