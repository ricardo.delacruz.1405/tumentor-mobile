import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchUsersPage } from './search-users';

import { SharedModule } from '../../shared/shared.module';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    SearchUsersPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(SearchUsersPage),
  ],
  providers: [
    Toast,
    Network
  ]
})
export class SearchUsersPageModule {}
