import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { SERVER } from '../../app/app.component';
import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabNewPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-new-post',
  templateUrl: 'tab-new-post.html',
})
export class TabNewPostPage {
  loggedUser: any;
  image: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private storage: Storage,
    private camera: Camera
  ) {
    this.loggedUser = {
      image: ''
    };
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  ionViewDidLoad() {

  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
    }, (err) => {
      // Handle error
    });
  }

  chooseImage() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // this.image = imageData;
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
    }, (err) => {
      // Handle error
    });
  }

  removeImage() {
    this.image = null;
  }

}
