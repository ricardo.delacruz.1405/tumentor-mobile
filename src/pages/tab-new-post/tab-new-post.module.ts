import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabNewPostPage } from './tab-new-post';

import { SharedModule } from '../../shared/shared.module';

import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    TabNewPostPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(TabNewPostPage),
  ],
  providers: [
    Camera
  ]
})
export class TabNewPostPageModule {}
