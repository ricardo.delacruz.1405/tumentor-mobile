import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabNotificationsPage } from './tab-notifications';

@NgModule({
  declarations: [
    TabNotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabNotificationsPage),
  ],
})
export class TabNotificationsPageModule {}
