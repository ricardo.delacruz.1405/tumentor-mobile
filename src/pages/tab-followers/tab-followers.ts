import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

import { RelationshipProvider } from '../../providers/relationship/relationship';

import { SERVER } from '../../app/app.component';
import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabFollowersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-followers',
  templateUrl: 'tab-followers.html',
})
export class TabFollowersPage {
  rootNavCtrl: NavController;
  isLoading: boolean;
  noResults: { value: boolean };
  user: any;
  loggedUser: any;
  relationships: any;

  constructor(
    private navParams: NavParams,
    private storage: Storage,
    private toast: Toast,
    private network: Network,
    private relationshipProvider: RelationshipProvider
  ) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    this.user = this.navParams.get('user');
    this.isLoading = true;
    this.relationships = {
      results: []
    };
    this.noResults = { value: true };
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
    let url = SERVER + '/relationship/relationship-api/';
    this.getFollowers(url, false, this.user.id);
  }

  ionViewDidLoad() {
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  ionViewWillEnter() {
    this.storage.get('removedFolloweds').then((val) => {
      if (val) {
        for (let item of val) {
          let foundItem = this.relationships.results.filter((relationship) => {
            if (relationship.follower == item.id) return true
          })
          if (foundItem.length > 0) {
            foundItem = foundItem[0];
            foundItem.obj_follower.relationship.is_following = !foundItem.obj_follower.relationship.is_following;
          }
        }
        this.storage.remove('removedFolloweds');
      }
    });

    this.storage.get('updateStatus').then((val) => {
      if (val) {
        let idRelationship;
        let foundItem = this.relationships.results.filter((relationship) => {
          let foundRelationship;
          for (let item of val) {
            if (item.followed == relationship.follower) {
              idRelationship = item.id;
              foundRelationship = item;
              break
            }
          }
          if (foundRelationship) return true
        })
        if (foundItem.length > 0) {
          foundItem = foundItem[0];
          foundItem.obj_follower.relationship.id = idRelationship;
          foundItem.obj_follower.relationship.is_following = true;
        }
        this.storage.remove('updateStatus');
      }
    });

    this.storage.get('deleteRelationship').then((val) => {
      if (val) {
        let foundItem = this.relationships.results.filter((relationship) => {
          let foundRelationship;
          for (let item of val) {
            if (item.followed == relationship.follower) {
              foundRelationship = item;
              break
            }
          }
          if (foundRelationship) return true
        })
        if (foundItem.length > 0) {
          foundItem = foundItem[0];
          foundItem.obj_follower.relationship.id = null;
          foundItem.obj_follower.relationship.is_following = false;
        }
        this.storage.remove('deleteRelationship');
      }
    });
  }

  getFollowers(url, isNewPage, idFollowed, text = null) {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      this.isLoading = true;
      this.relationshipProvider.getFollowers(url, isNewPage, idFollowed).subscribe(
        data => {
          if (this.relationships.next) {
            let newRelationships = data['results'];
            this.relationships.next = data['next'];
            for (let relationship of newRelationships) {
              relationship['updateRelationship'] = true;
              if (this.loggedUser.id == relationship.follower) relationship['isVisible'] = false;
              else relationship['isVisible'] = true;
              this.relationships.results.push(relationship);
            }
          } else {
            this.relationships = data;
            for (let relationship of this.relationships.results) {
              relationship['updateRelationship'] = true;
              if (this.loggedUser.id == relationship.follower) relationship['isVisible'] = false;
              else relationship['isVisible'] = true;
            }
          }
          if (this.relationships.results.length == 0) this.noResults.value = true;
          else this.noResults.value = false;
          this.isLoading = false;
        },
        err => {
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
          this.rootNavCtrl.pop();
        }
      );
    }
  }

  goProfile(user) {
    if (user.id == this.loggedUser.id) {
      this.rootNavCtrl.push('TabAccountPage');
    } else {
      this.rootNavCtrl.push('UserProfilePage', {
        idUser: user.id,
        rootUser: this.navParams.get('rootUser'),
        rootTextFolloweds: this.navParams.get('rootTextFolloweds'),
        user: this.navParams.get('user'),
        textFolloweds: this.navParams.get('textFolloweds'),
        relationships: this.relationships,
        noResults: this.noResults,
        removeFromList: false
      });
    }
  }

  followUser(relationship) {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      relationship.updateRelationship = false;
      relationship.obj_follower.relationship.is_following = !relationship.obj_follower.relationship.is_following;
      let body = {
        followed: relationship.obj_follower.id
      }
      this.relationshipProvider.followUser(body).subscribe(
        data => {
          relationship.obj_follower.relationship.id = data['id'];
          relationship.updateRelationship = true;
          data['updateRelationship'] = true;
          data['isVisible'] = true;
          if (this.user.id == this.loggedUser.id) {
            this.user.num_followeds += 1;
            let textFolloweds = this.navParams.get('textFolloweds');
            if (textFolloweds) {
              textFolloweds.content = this.user.num_followeds.toString();
              if (this.user.num_followeds == 1) textFolloweds.content += ' seguido';
              else textFolloweds.content += ' seguidos';
            }

            this.storage.get('addedFolloweds').then((val) => {
              if (val) {
                val.push(data);
                this.storage.set('addedFolloweds', val);
              } else {
                this.storage.set('addedFolloweds', [data]);
              }
            });
          } else {
            this.storage.get('updateStatus').then((val) => {
              if (val) {
                let foundRelationship = val.filter((item) => {
                  if (item.id == data['id']) return true
                })
                if (foundRelationship.length == 0) {
                  val.push(data);
                  this.storage.set('updateStatus', val);
                }
              } else {
                this.storage.set('updateStatus', [data]);
              }
            });
            let rootUser = this.navParams.get('rootUser');
            if (rootUser) {
              rootUser.num_followeds += 1;
              let rootTextFolloweds = this.navParams.get('rootTextFolloweds');
              if (rootTextFolloweds) {
                rootTextFolloweds.content = rootUser.num_followeds.toString();
                if (rootUser.num_followeds == 1) rootTextFolloweds.content += ' seguido';
                else rootTextFolloweds.content += ' seguidos';
              }
            }
          }

        },
        err => {
          relationship.obj_follower.relationship.is_following = !relationship.obj_follower.relationship.is_following;
          relationship.updateRelationship = true;
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
        }
      )
    }
  }

  unfollowUser(relationship) {
    if (this.network.type === 'none') {
      this.showToast('Conéctate a Internet', 4500);
    } else {
      relationship.updateRelationship = false;
      relationship.obj_follower.relationship.is_following = !relationship.obj_follower.relationship.is_following;
      this.relationshipProvider.unfollowUser(relationship.obj_follower.relationship.id).subscribe(
        data => {
          relationship.updateRelationship = true;

          if (this.user.id == this.loggedUser.id) {
            this.user.num_followeds -= 1;
            let textFolloweds = this.navParams.get('textFolloweds');
            if (textFolloweds) {
              textFolloweds.content = this.user.num_followeds.toString();
              if (this.user.num_followeds == 1) textFolloweds.content += ' seguido';
              else textFolloweds.content += ' seguidos';
            }
            this.storage.get('removedFolloweds').then((val) => {
              if (val) {
                val.push(relationship.obj_follower);
                this.storage.set('removedFolloweds', val);
              } else {
                this.storage.set('removedFolloweds', [relationship.obj_follower]);
              }
            });
          } else {
            this.storage.get('deleteRelationship').then((val) => {
              if (val) {
                let foundRelationship = val.filter((item) => {
                  if (item.id == relationship.id) return true
                })
                if (foundRelationship.length == 0) {
                  val.push(relationship);
                  this.storage.set('deleteRelationship', val);
                }
              } else {
                this.storage.set('deleteRelationship', [relationship]);
              }
            });

            let rootUser = this.navParams.get('rootUser');
            if (rootUser) {
              rootUser.num_followeds -= 1;
              let rootTextFolloweds = this.navParams.get('rootTextFolloweds');
              if (rootTextFolloweds) {
                rootTextFolloweds.content = rootUser.num_followeds.toString();
                if (rootUser.num_followeds == 1) rootTextFolloweds.content += ' seguido';
                else rootTextFolloweds.content += ' seguidos';
              }
            }
          }

        },
        err => {
          relationship.obj_follower.relationship.is_following = !relationship.obj_follower.relationship.is_following;
          relationship.updateRelationship = true;
          this.showToast('¡Uy! Ocurrió un problema, inténtelo nuevamente', 5000);
        }
      )
    }
  }

  showToast(
    msg: string,
    duration: number = 4500,
    position: string = 'bottom'
  ) {
    this.toast.showWithOptions({
      message: msg,
      duration: duration,
      position: position,
      addPixelsY: -120,
    }).subscribe(toast => { });
  }

}
