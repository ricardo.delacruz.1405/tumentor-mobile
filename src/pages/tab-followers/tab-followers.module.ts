import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabFollowersPage } from './tab-followers';

import { SharedModule } from '../../shared/shared.module';

import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    TabFollowersPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(TabFollowersPage),
  ],
  providers: [
    Toast,
    Network
  ]
})
export class TabFollowersPageModule {}
