import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TabSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-search',
  templateUrl: 'tab-search.html',
})
export class TabSearchPage {
  @ViewChild('searchBar') searchBar: any;
  rootNavCtrl: NavController;

  constructor(
    private navParams: NavParams
  ) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.searchBar._searchbarInput.nativeElement.readOnly = true;
    }, 600);
  }

  goSearch() {
    this.rootNavCtrl.push('SearchUsersPage');
  }

}
