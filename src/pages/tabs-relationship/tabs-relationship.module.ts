import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsRelationshipPage } from './tabs-relationship';

import { SuperTabsModule } from 'ionic2-super-tabs';

@NgModule({
  declarations: [
    TabsRelationshipPage,
  ],
  imports: [
    SuperTabsModule,
    IonicPageModule.forChild(TabsRelationshipPage),
  ],
})
export class TabsRelationshipPageModule {}
