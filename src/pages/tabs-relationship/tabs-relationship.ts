import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SuperTabsController } from 'ionic2-super-tabs';

import { SharedPage } from '../../shared/shared';

/**
 * Generated class for the TabsRelationshipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-relationship',
  templateUrl: 'tabs-relationship.html',
  providers: [SuperTabsController]
})
export class TabsRelationshipPage {
  tab1Root = 'TabFollowersPage';
  tab2Root = 'TabFollowedsPage';
  params: any;
  user: any;
  loggedUser: any;
  textFollowers: string;
  textFolloweds: string;

  constructor(
    private navParams: NavParams,
    private storage: Storage,
    private superTabsCtrl: SuperTabsController
  ) {
    this.params = this.navParams.data;
    this.user = this.navParams.get('user');
    this.textFollowers = this.navParams.get('textFollowers');
    this.textFolloweds = this.navParams.get('textFolloweds');
  }
  
  ionViewDidLoad() {
    let shared: SharedPage;
    shared = new SharedPage(this.storage);
    shared.getLoggedUser().then((val) => {
      this.setLoggedUser(val);
    });
  }

  setLoggedUser(loggedUser) {
    this.loggedUser = loggedUser;
  }

  ngAfterViewInit() {
    this.superTabsCtrl.slideTo(this.navParams.get('indexTab'));
  }

}
