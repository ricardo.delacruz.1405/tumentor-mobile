import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { AppMinimize } from '@ionic-native/app-minimize';
import { SharedPage } from '../shared/shared';

export var SERVER = 'http://192.168.1.38:8000';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string;
  @ViewChild(Nav) nav: Nav;

  constructor(
    platform: Platform,
    storage: Storage,
    statusBar: StatusBar,
    appMinimize: AppMinimize,
    app: App
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      let shared: SharedPage;
      shared = new SharedPage(storage);
      shared.getLoggedUser().then((val) => {
        this.setLoggedUser(val);
      });

      // App minimize
      if (this.nav) {
        platform.registerBackButtonAction(() => {
          let nav = app._appRoot._getActivePortal() || app.getActiveNav();
          let activeView = nav.getActive();
          if (activeView.isOverlay) {
            activeView.dismiss();
          } else {
            if (this.nav.getActive().id == 'TabsHomePage' || this.nav.getActive().id == 'WelcomePage') {
              appMinimize.minimize();
            }
            else {
              this.nav.pop();
            }
          }
        });
      }
    });
  }

  setLoggedUser(loggedUser) {
    if (loggedUser) this.rootPage = 'TabsHomePage';
    else this.rootPage = 'WelcomePage';
  }
}

